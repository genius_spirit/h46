$(function () {
  let url = 'https://pokeapi.co/api/v2/pokemon/';
  var globalUrl;

  $(document).ajaxStart(function () {
    $('#preloader').fadeIn(500);
  });
  $(document).ajaxStop(function () {
    $('#preloader').fadeOut(500);
  });

  let showPokemonsList = function (pokemonsNames) {
    let container = $('.pokemonsList');
    container.empty();
    let li = pokemonsNames.map(function (pokemons, index) {
      return $('<li data-id="' + index + '">').text(pokemons.name).on('click', getInfoAboutPokemon(pokemons.url));
    });
    let ol = $('<ol>').append(li);
    container.append('<h3>Pokemons List:</h3>', ol);
  };

  let getInfoAboutPokemon = function (url) {
    return async function () {
      showPokemon(await $.ajax(url));
    };
  };

  let pokemonsList = async function (url) {
    globalUrl = await $.ajax(url);
    console.log(globalUrl);
    showPokemonsList(globalUrl.results);
    nextList(globalUrl.next);
  };

  pokemonsList(url);

  let showPokemon = function (result) {
    let container = $('.aboutPokemon');
    container.empty();
    container.append(`<img src="${result.sprites.front_default}">`);
    container.append(`<div>Name: ${result.name}`);
    container.append(`<div>Height: ${result.height}`);
    container.append(`<div>Weight: ${result.weight}`);
    (result.types).forEach((item) => {
      container.append('<div>Type:' + [item][0].type.name + '<br>');
    });
  };

  function nextList(url){
    $('#next').on('click', async function() {
      globalUrl = await $.ajax(url);
      console.log(globalUrl);
      showPokemonsList(globalUrl.results);
    })
  }

});
